package com.nkorth.music;

import android.os.AsyncTask;
import android.util.Log;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class LoadLibraryTask extends AsyncTask<File, Integer, List<Album>> {
    @Override
    protected List<Album> doInBackground(File[] searchLocations) {
        ArrayList<Album> albums = new ArrayList<>();
        for(File dir : searchLocations) {
            traverse(dir, albums);
        }
        Collections.sort(albums, new Comparator<Album>() {
            @Override
            public int compare(Album lhs, Album rhs) {
                return lhs.title.compareTo(rhs.title);
            }
        });
        return albums;
    }

    public void traverse(File dir, List<Album> albums){
        Log.w("searching dir", dir.getPath());
        File[] files = dir.listFiles();
        if(files != null) {
            for (File file : files) {
                String fname = file.getName();
                if(fname.equals(".") || fname.equals("..")){
                    continue;
                }
                if (file.isDirectory()) {
                    traverse(file, albums);
                } else if (fname.length() > 4 && fname.substring(fname.length()-4).equals(".cue")) {
                    try {
                        Album a = new Album(file);
                        albums.add(a);
                    } catch(IOException e) {
                        Log.e("BuildLibraryTask", "Couldn't add album", e);
                    }
                }
            }
        }
    }
}
