package com.nkorth.music;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.MediaMetadata;
import android.media.session.MediaController;
import android.media.session.MediaSession;
import android.media.session.PlaybackState;
import android.util.Log;

public class PlaybackNotificationCallbacks extends MediaController.Callback {

    private Context context;
    private MediaSession.Token token;
    private int state;
    private MediaMetadata metadata;

    public PlaybackNotificationCallbacks(Context context, MediaSession.Token token){
        this.context = context;
        this.token = token;
    }

    @Override
    public void onPlaybackStateChanged(PlaybackState state) {
        Log.w("NotificationCallbacks", "playbackStateChanged");
        this.state = state.getState();
        updateNotification();
        super.onPlaybackStateChanged(state);
    }

    @Override
    public void onMetadataChanged(MediaMetadata metadata) {
        Log.w("NotificationCallbacks", "metadata");
        this.metadata = metadata;
        updateNotification();
        super.onMetadataChanged(metadata);
    }

    @Override
    public void onSessionDestroyed() {
        ((NotificationManager)context.getSystemService(Context.NOTIFICATION_SERVICE)).cancelAll();
        super.onSessionDestroyed();
    }

    private void updateNotification(){
        PendingIntent open_pi = PendingIntent.getActivity(context, 0,
                new Intent(context, MainActivity.class),
                PendingIntent.FLAG_UPDATE_CURRENT);
        PendingIntent stop_pi = PendingIntent.getService(context, 0,
                new Intent(PlaybackService.ACTION_STOP, null, context, PlaybackService.class),
                PendingIntent.FLAG_UPDATE_CURRENT);

        Notification.Builder notification = new Notification.Builder(context)
                .setPriority(Notification.PRIORITY_HIGH)
                .setShowWhen(false)
                .setSmallIcon(R.drawable.notification_icon)
                .setVisibility(Notification.VISIBILITY_PUBLIC)
                .setContentIntent(open_pi)
                .setDeleteIntent(stop_pi)
                .setStyle(new Notification.MediaStyle()
                        .setMediaSession(token)
                        .setShowActionsInCompactView(1));
        notification.addAction(buildAction(PlaybackService.ACTION_PREVIOUS,
                android.R.drawable.ic_media_previous, "Skip back"));
        if(state == PlaybackState.STATE_PLAYING) {
            notification.addAction(buildAction(PlaybackService.ACTION_PAUSE,
                    android.R.drawable.ic_media_pause, "Pause"));
        } else{
            notification.addAction(buildAction(PlaybackService.ACTION_PLAY,
                    android.R.drawable.ic_media_play, "Play"));
        }
        notification.addAction(buildAction(PlaybackService.ACTION_NEXT,
                android.R.drawable.ic_media_next, "Skip forward"));
        if(metadata != null){
            notification.setContentTitle(metadata.getString(MediaMetadata.METADATA_KEY_TITLE));
            notification.setContentText(metadata.getString(MediaMetadata.METADATA_KEY_ALBUM));
            notification.setLargeIcon(metadata.getBitmap(MediaMetadata.METADATA_KEY_ALBUM_ART));
        } else{
            notification.setContentTitle(context.getText(R.string.not_playing));
            notification.setContentText("");
            notification.setLargeIcon(null);
        }
        notification.setOngoing(state == PlaybackState.STATE_PLAYING);
        ((NotificationManager)context.getSystemService(Context.NOTIFICATION_SERVICE))
                .notify(0, notification.build());
    }

    private Notification.Action buildAction(String action, int icon, String title){
        PendingIntent pi = PendingIntent.getService(context, 0,
                new Intent(action, null, context, PlaybackService.class),
                PendingIntent.FLAG_UPDATE_CURRENT);
        return new Notification.Action.Builder(icon, title, pi).build();
    }
}
