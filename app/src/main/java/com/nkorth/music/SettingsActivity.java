package com.nkorth.music;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.preference.Preference;
import android.preference.PreferenceActivity;

import java.io.File;

public class SettingsActivity extends PreferenceActivity {
    private static final String KEY_MUSIC_FOLDER = "musicDir";
    public static final String KEY_SHUFFLE_ALBUMS = "shuffleAlbums";
    public static final String KEY_USE_EXTERNAL = "useExternal";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.preferences);

        Preference musicDir = findPreference(KEY_MUSIC_FOLDER);
        File musicDirFile = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_MUSIC);
        musicDir.setSummary(musicDirFile.getPath());
        Intent i = new Intent(Intent.ACTION_VIEW, Uri.fromFile(musicDirFile));
        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        musicDir.setIntent(i);

        Preference useExternal = findPreference(KEY_USE_EXTERNAL);
        if(getExternalStorageDir() != null){
            useExternal.setSummary(getExternalStorageDir()+"/Music");
        } else{
            useExternal.setEnabled(false);
        }
    }

    public static String getExternalStorageDir(){
        String externalDirs = System.getenv("SECONDARY_STORAGE");
        if(externalDirs != null && externalDirs.indexOf(':') != -1) {
            return externalDirs.substring(0, externalDirs.indexOf(':'));
        } else{
            return externalDirs;
        }
    }
}
