package com.nkorth.music;

import android.net.Uri;
import android.os.AsyncTask;
import android.util.Log;

import java.io.File;
import java.io.IOException;

public class LoadAlbumTask extends AsyncTask<Uri, Integer, Album> {
    @Override
    protected Album doInBackground(Uri... locations) {
        try {
            return new Album(new File(locations[0].getPath()));
        } catch(IOException e){
            Log.e("LoadAlbumTask", "failed to load album", e);
            return null;
        }
    }
}
