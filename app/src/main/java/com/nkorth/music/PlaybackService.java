package com.nkorth.music;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.session.MediaController;
import android.media.session.MediaSession;
import android.media.session.PlaybackState;
import android.net.Uri;
import android.os.Environment;
import android.os.Handler;
import android.os.IBinder;
import android.preference.PreferenceManager;
import android.util.Log;

import java.io.File;
import java.io.IOException;
import java.util.List;

public class PlaybackService extends Service implements MediaPlayer.OnPreparedListener, MediaPlayer.OnCompletionListener {
    public static final String ACTION_PLAY = "com.nkorth.music.PLAY";
    public static final String ACTION_PAUSE = "com.nkorth.music.PAUSE";
    public static final String ACTION_NEXT = "com.nkorth.music.NEXT";
    public static final String ACTION_PREVIOUS = "com.nkorth.music.PREVIOUS";
    public static final String ACTION_STOP = "com.nkorth.music.STOP";

    private MediaPlayer player;
    private MediaSession session;
    private MediaController controller;

    private Album currentAlbum;
    private UnplugReceiver unplugReceiver;

    @Override
    public void onCreate() {
        super.onCreate();
        player = new MediaPlayer();
        player.setOnCompletionListener(this);
        player.setOnPreparedListener(this);

        initSession();
        initController();

        unplugReceiver = new UnplugReceiver();
        registerReceiver(unplugReceiver, new IntentFilter(AudioManager.ACTION_AUDIO_BECOMING_NOISY));
    }

    private void initSession(){
        session = new MediaSession(this, "PlaybackService");
        session.setSessionActivity(PendingIntent.getActivity(this, 0,
                new Intent(this, MainActivity.class),
                PendingIntent.FLAG_UPDATE_CURRENT));
        session.setFlags(MediaSession.FLAG_HANDLES_TRANSPORT_CONTROLS);
        session.setCallback(new MediaSession.Callback(){
            @Override
            public void onPlay() {
                if (currentAlbum != null) {
                    player.start();
                    updatePlaybackState();
                }
                super.onPlay();
            }

            @Override
            public void onPause() {
                if (currentAlbum != null) {
                    player.pause();
                    updatePlaybackState();
                    saveResumeTime();
                }
                super.onPause();
            }

            @Override
            public void onSkipToNext() {
                if(currentAlbum != null){
                    skipTrack(1);
                    updatePlaybackState();
                    saveResumeTime();
                }
                super.onSkipToNext();
            }

            @Override
            public void onSkipToPrevious() {
                if(currentAlbum != null){
                    skipTrack(-1);
                    updatePlaybackState();
                    saveResumeTime();
                }
                super.onSkipToPrevious();
            }

            @Override
            public void onStop() {
                super.onStop();
                saveResumeTime();
                stopSelf();
            }
        });
        session.setActive(true);
    }

    private void initController(){
        controller = new MediaController(this, session.getSessionToken());
        controller.registerCallback(new PlaybackNotificationCallbacks(this, session.getSessionToken()));
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if(intent != null) {
            switch (intent.getAction()) {
                case ACTION_PLAY:
                    if (intent.getData() != null){
                        setCurrentAlbum(intent.getData());
                        // will automatically play once ready
                    } else {
                        controller.getTransportControls().play();
                    }
                    break;
                case ACTION_PAUSE:
                    controller.getTransportControls().pause();
                    break;
                case ACTION_PREVIOUS:
                    controller.getTransportControls().skipToPrevious();
                    break;
                case ACTION_NEXT:
                    controller.getTransportControls().skipToNext();
                    break;
                case ACTION_STOP:
                    controller.getTransportControls().stop();
                    break;
            }
        } else{
            Log.e("PlaybackService", "null intent");
        }
        return START_NOT_STICKY;
    }

    private void updatePlaybackState(){
        PlaybackState.Builder s = new PlaybackState.Builder();
        if(player.isPlaying()){
            s.setState(PlaybackState.STATE_PLAYING, player.getCurrentPosition(), 1);
            s.setActions(PlaybackState.ACTION_SKIP_TO_NEXT | PlaybackState.ACTION_SKIP_TO_PREVIOUS | PlaybackState.ACTION_PAUSE | PlaybackState.ACTION_STOP);
        } else{
            s.setState(PlaybackState.STATE_PAUSED, player.getCurrentPosition(), 0);
            s.setActions(PlaybackState.ACTION_SKIP_TO_NEXT | PlaybackState.ACTION_SKIP_TO_PREVIOUS | PlaybackState.ACTION_PLAY | PlaybackState.ACTION_STOP);
        }
        session.setPlaybackState(s.build());
    }

    private Handler nextTrackHandler = new Handler();

    private void updateMetadata(final int trackNumber){
        session.setMetadata(currentAlbum.getMetadata(trackNumber));

        // schedule next track update
        if(trackNumber + 1 < currentAlbum.tracks.size()) {
            int currentPos = player.getCurrentPosition();
            int nextPos = currentAlbum.tracks.get(trackNumber + 1).pregap;
            if(nextPos == 0){
                nextPos = currentAlbum.tracks.get(trackNumber + 1).position;
            }
            int nextDelay = nextPos - currentPos;
            nextTrackHandler.removeCallbacksAndMessages(null);
            nextTrackHandler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    updateMetadata(trackNumber + 1);
                }
            }, nextDelay);
            Log.w("PlaybackService", "scheduled next track in "+nextDelay+"ms");
        }
    }

    private void setCurrentAlbum(Uri cuesheet){
        if(currentAlbum == null || !cuesheet.equals(currentAlbum.cuesheet)) {
            saveResumeTime();
            new LoadAlbumTask(){
                @Override
                protected void onPostExecute(Album album) {
                    currentAlbum = album;
                    player.reset();
                    try {
                        player.setDataSource(PlaybackService.this, currentAlbum.audio);
                        player.prepareAsync();
                    } catch(IOException e){
                        Log.e("PlaybackService", "failed to play album", e);
                        currentAlbum = null;
                    }
                }
            }.execute(cuesheet);
        } else{
            controller.getTransportControls().play();
        }
    }

    private void skipTrack(int offset){
        int currentTrack = currentAlbum.findCurrentTrack(player.getCurrentPosition());
        int newTrack = currentTrack + offset;
        if (newTrack < 0) {
            player.seekTo(0);
        } else if (newTrack >= currentAlbum.tracks.size()) {
            controller.getTransportControls().pause();
            player.seekTo(0);
        } else {
            player.seekTo(currentAlbum.tracks.get(newTrack).position);
        }
        updateMetadata(newTrack);
    }

    @Override
    public void onPrepared(MediaPlayer mp) {
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(this);
        int time = sp.getInt("resumeTime;"+currentAlbum.cuesheet.toString(), 0);
        mp.seekTo(time);
        updateMetadata(currentAlbum.findCurrentTrack(time));
        controller.getTransportControls().play();
    }

    private void saveResumeTime(){
        if(currentAlbum != null && player.getCurrentPosition() > 0){
            SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(this);
            sp.edit()
                    .putInt("resumeTime;"+currentAlbum.cuesheet.toString(), player.getCurrentPosition())
                    .apply();
            Log.w("PlaybackService", "saving "+player.getCurrentPosition());
        }
    }

    @Override
    public void onCompletion(MediaPlayer mp) {
        Log.w("PlaybackService", "onCompletion");
        player.seekTo(0);
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(this);
        if(sp.getBoolean(SettingsActivity.KEY_SHUFFLE_ALBUMS, false)){
            LoadLibraryTask task = new LoadLibraryTask(){
                @Override
                protected void onPostExecute(List<Album> albums) {
                    Album randomAlbum = albums.get((int)(Math.random()*albums.size()));
                    setCurrentAlbum(randomAlbum.cuesheet);
                }
            };
            if(sp.getBoolean(SettingsActivity.KEY_USE_EXTERNAL, false)) {
                task.execute(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_MUSIC),
                        new File(SettingsActivity.getExternalStorageDir()));
            } else{
                task.execute(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_MUSIC));
            }
        } else {
            controller.getTransportControls().stop();
        }
    }

    private class UnplugReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            if(AudioManager.ACTION_AUDIO_BECOMING_NOISY.equals(intent.getAction())){
                controller.getTransportControls().pause();
            }
        }
    }

    private void tearDown(){
        session.release();
        saveResumeTime();
        ((NotificationManager)getSystemService(Context.NOTIFICATION_SERVICE)).cancelAll();
        unregisterReceiver(unplugReceiver);
    }

    @Override
    public void onTaskRemoved(Intent rootIntent) {
        tearDown();
        super.onTaskRemoved(rootIntent);
    }

    @Override
    public void onDestroy() {
        tearDown();
        super.onDestroy();
    }

    @Override
    public IBinder onBind(Intent intent) {
        throw new RuntimeException("not implemented");
    }
}
